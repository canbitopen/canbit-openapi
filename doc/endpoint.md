# Api Endpoints for Broker

## Base Domain

canbit.com

## API Endpoint ($HOST)

| Name               | base endpoint                                    |
| ------------------ | ------------------------------------------------ |
| rest-api           | **[https://api.canbit.com](https://api.canbit.com)** |
| web-socket-streams | **[wss://wsapi.canbit.com](wss://wsapi.canbit.com)** |
| user-data-stream   | **[wss://wsapi.canbit.com](wss://wsapi.canbit.com)** |
